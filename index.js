/**
 * @description - WorkingIn Coding Challenge
 * @author - Kumarjit Chakraborty
 * ***************************************************************************************************
 * @method - generateSequence
 * @description - This mmethod generates the sequence; Main method of the solution;
 * Answers:
 * 1. What is the 15th element of the sequent
 * Ans: 1220
 * 2. What is your solution time and space complexity, regarding for the n(th) element of the sequent?
 * Ans: 
 *    - Time Complexity is O(n); Which is linear.
 *    - Space Complexity is - O(n); Since no additional variables are created
 * 3. What clean code principles you have been used, and why?
 * Ans:
 *    - KISS - Keep it simple and stupid
 *    - I have 2 jargons made up by myself
 *      - Fat free code - Do not use a variable if it is not absolutely neccessary
 *      - Linear code - The less indented code is more readable; Minimalistic use of block(condition or
 *        loop)
 * 4. Can you make your code recursive? If yes, what would be your time and space complexity?
 * Ans:
 *    - I have made it recursive
 *    - It will accept the starting number and sequence length and geenerate the desired element
 *    - I have printed the series and nth element; This can be used in different ways
 *    - I have created a placeholder HTML only for this purppose
 *    - Complexity of the recursive solution:
 *      # It is the same as O(n)
 * ***************************************************************************************************
 * @method - printResult
 * @description - Helper method to display the result
 * ***************************************************************************************************
 * @method - onGenerateClick
 * @description - Helper method to handle generate button click 
 * ***************************************************************************************************
 * @method - onClearClick
 * @description - Helper method to handle clear button click 
 * ***************************************************************************************************
 */
"use strict";

const DEFAULT_STARTING_NUMBER = 2;
const DEFAULT_SEQUENCE_LENGTH = 15;
const DEFAULT_MESSAGE = 'Result Will Appear Here';

let printResult = (resultMessage) => {
  if (typeof resultMessage === 'string') {
    document.getElementById('message').innerHTML = resultMessage;
  } else {
    let messageELement = document.getElementById('message');
    messageELement.innerHTML = '';
    resultMessage.forEach((message, index) => {
      let lineBreak = index ? '<br><br>' : '';
      messageELement.innerHTML += lineBreak + message;
    });
  }
}

let generateSequence = (start, sequence) => {
  let series = [];

  start = start || DEFAULT_STARTING_NUMBER;
  sequence = sequence || DEFAULT_SEQUENCE_LENGTH;

  if (!series.length) {
    series = [start, start];
  }

  while (series.length < sequence) {
    series.push(series[series.length - 1] + series[series.length - 2]);
  }

  printResult([
    `${sequence}th Element: ${series[sequence - 1]}`,
    `Series: ${series}`
  ]);
}

let onGenerateClick = (event) => {
  event.preventDefault();

  let start = document.getElementById('startingNumber').value * 1 || DEFAULT_STARTING_NUMBER;
  let sequence = document.getElementById('sequence').value * 1 || DEFAULT_SEQUENCE_LENGTH;

  generateSequence(start, sequence);
}

let onClearClick = () => {
  document.getElementById('message').innerHTML = DEFAULT_MESSAGE;
}

onClearClick();